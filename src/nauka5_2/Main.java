package nauka5_2;

import java.util.Scanner;

class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int program;
        do {
            System.out.println("Witamy w sklepie, co podać");
            System.out.println("1. mleko (3zl)");
            System.out.println("2. papierosy (14zl)");
            System.out.println("3. jajka (7zl)");
            System.out.println("4. slodycze (5zl)");
            System.out.println("5. chrupki (6zl)");
            Scanner cyfra = new Scanner(System.in);
            int wybor = cyfra.nextInt();
            System.out.println("Wybrałes pozycje numer: " + wybor);
            if (wybor >=6) {
                System.out.println("Nie ma tego na liscie");

            }
            if (wybor == 1) {
                System.out.println("Prosze bardzo, oto twoje mleko. Placisz jakim nominalem? (5, 10, 20)");
                Scanner nominal = new Scanner(System.in);
                int kasa = nominal.nextInt();
                if (kasa == 5) {
                    System.out.println("Twoja reszta to 2zl");
                } else if (kasa == 10) {
                    System.out.println("Twoja reszta to 7zl");
                } else if (kasa == 20) {
                    System.out.println("Twoja reszta to 17zl");
                } else if (kasa != 5 || kasa != 10 || kasa != 20) {
                    System.out.println("Przyjmujemy tylko nominaly 5, 10 i 20zl");
                }
            }
            if (wybor == 2) {
                System.out.println("Prosze bardzo, oto twoje papierosy. Placisz jakim nominalem? (5, 10, 20)");
                Scanner nominal = new Scanner(System.in);
                int kasa = nominal.nextInt();
                if (kasa == 5) {
                    System.out.println("To za malo o 8zl!!!");
                } else if (kasa == 10) {
                    System.out.println("To za malo o 4zl!!!");
                } else if (kasa == 20) {
                    System.out.println("Twoja reszta to 6zl. Nie szkoda ci zdrowia?");
                }else if (kasa != 5 || kasa != 10 || kasa != 20) {
                    System.out.println("Przyjmujemy tylko nominaly 5, 10 i 20zl");
                }
            }
            if (wybor == 3) {
                System.out.println("Prosze bardzo, oto twoje jajka. Placisz jakim nominalem? (5, 10, 20)");
                Scanner nominal = new Scanner(System.in);
                int kasa = nominal.nextInt();
                if (kasa == 5) {
                    System.out.println("To za malo o 2zl!!!");
                } else if (kasa == 10) {
                    System.out.println("Twoja reszta to 2zl, solisz jajka?");
                } else if (kasa == 20) {
                    System.out.println("Twoja reszta to 12zl. Sadzone najlepsze!");
                }else if (kasa != 5 || kasa != 10 || kasa != 20) {
                    System.out.println("Przyjmujemy tylko nominaly 5, 10 i 20zl");
                }
            }
            if (wybor == 4) {
                System.out.println("Prosze bardzo, oto twoje cukierasy. Placisz jakim nominalem? (5, 10, 20)");
                Scanner nominal = new Scanner(System.in);
                int kasa = nominal.nextInt();
                if (kasa == 5) {
                    System.out.println("Równiutko 5zl, bez reszty");
                } else if (kasa == 10) {
                    System.out.println("5zl reszty dla ciebie, polecam ograniczyc slodycze");
                } else if (kasa == 20) {
                    System.out.println("Twoja reszta to 15zl. Poszedlbys na silke");
                }else if (kasa != 5 || kasa != 10 || kasa != 20) {
                    System.out.println("Przyjmujemy tylko nominaly 5, 10 i 20zl");
                }
            }
            if (wybor == 5) {
                System.out.println("Prosze bardzo, oto twoje chrupeczki. Placisz jakim nominalem? (5, 10, 20)");
                Scanner nominal = new Scanner(System.in);
                int kasa = nominal.nextInt();
                if (kasa == 5) {
                    System.out.println("Zeta dorzuć to moze ci sprzedam, nie targuje sie");
                } else if (kasa == 10) {
                    System.out.println("4zl reszty dla ciebie, to juz czwarta paczka dzisiaj...");
                } else if (kasa == 20) {
                    System.out.println("Twoja reszta to 14zl. A moze salatka nastepnym razem?");
                }else if (kasa != 5 || kasa != 10 || kasa != 20) {
                    System.out.println("Przyjmujemy tylko nominaly 5, 10 i 20zl");
                }
            }
            System.out.println("Jesli chcesz wyjsc wpisz 0, jesli chcesz kupic cos jeszcze wcisnij 1");
            program = input.nextInt();
        } while (program != 0);
        System.out.println("Do nastepnego!");
    }
}
